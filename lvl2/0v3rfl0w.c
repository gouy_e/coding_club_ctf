int		main()
{
  char		buff[512];
  int		i;

  i = 0;

  ...
  
  read(0, buff, ...);

  ...

  if (i != 0)
    print_passwd();
}
